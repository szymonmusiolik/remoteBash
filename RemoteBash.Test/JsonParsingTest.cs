/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <szymonmusiolik@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Szymon
 * ----------------------------------------------------------------------------
 */
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RemoteBash;
using System;
using System.Collections.Generic;
namespace RemoteBash.Test
{
    [TestClass]
    public class JsonParsingTest
    {
        [TestMethod]
        public void BasicBashCommand()
        {
            var result = "ps".Bash();
            Assert.IsNotNull(result);
            Assert.IsFalse(String.IsNullOrWhiteSpace(result.Result));
            Assert.IsTrue(String.IsNullOrWhiteSpace(result.Error));
        }

        [TestMethod]
        public void testingBashCommandsWorks()
        {
            foreach (var command in testingBashCommands)
            {
                var result = command.Bash();
                Assert.IsNotNull(result);
                Assert.IsTrue(String.IsNullOrWhiteSpace(result.Error), "error was not empty for command: " + command + "\nerror message: " + result.Error);
                Assert.IsFalse(String.IsNullOrWhiteSpace(result.Result), "result was empty for command: " + command);
            }

        }

        [TestMethod]
        public void testingParsingCommandsWorks()
        {
            foreach (var command in testingBashCommands)
            {
                var result = command.Bash();

                JsonParser parser = new JsonParser(command);
                var json = parser.GetJson(result);
                Assert.IsNotNull(json);
                Assert.IsFalse(String.IsNullOrWhiteSpace(json), "json was empty for command: " + command);
                Assert.IsFalse(json == "[]", "json was an empty json for command: " + command);
                Assert.IsFalse(json.Contains("Exception"), "Exception was thrown for command: " + command + "\nJSON result: " + json);
                Assert.IsTrue(json.Contains("\"Errors\":{\"Error\":\"\"}"), "Error occured for command: " + command + "\nJSON result: " + json);
            }

        }
        List<string> testingBashCommands = new List<string>()
        {
            //"top -n 1",
            "top -n 1 -b", //have to add -b, becouse it was not working in runtime
            "ifconfig -a",
            "ps -eo user,group,comm,args,pid,ppid,pgid,tty,vsz,stat,rss",
            "ps -e --forest",
            "lsblk -a",
            "df",
            "free",
            "netstat -a",
            "dpkg -l",
            
            "ls /proc",
            "ls /sys",
            
            "uname -r",
            "uname -a",
            "lscpu",
            "iostat -m",
            "vmstat",
            
            "cat /proc/diskstats",
            "cat /proc/stat",
            "cat /proc/uptime",
            "cat /proc/partitions",
            "cat /proc/version",
            "cat /proc/meminfo",
            "cat /proc/self/mountstats",
            "cat /proc/self/mounts"

        };
    }
}
