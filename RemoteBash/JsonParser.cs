/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <szymonmusiolik@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Szymon
 * ----------------------------------------------------------------------------
 */

using System;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace RemoteBash
{
    public class JsonParser
    {

        string _bashCommand;
        public JsonParser(string bashCommand)
        {
            _bashCommand = bashCommand;
        }
        public string GetJson(BashResult bashResult)
        {
            var parsed = new Dictionary<string, object>();
            var errorsDictionary = new Dictionary<string, string>();
            if (!String.IsNullOrWhiteSpace(bashResult.Result))
            {
                try
                {
                    if (_bashCommand.StartsWith("ps") ||
                        _bashCommand.StartsWith("lsblk") ||
                        _bashCommand.StartsWith("df") ||
                        _bashCommand.StartsWith("cat /proc/partitions"))
                    {
                        parsed.Add(_bashCommand, ParseCsvLike(bashResult));
                    }
                    else if (_bashCommand.StartsWith("top"))
                    {
                        parsed.Add(_bashCommand, ParseTop(bashResult));
                    }
                    else if (_bashCommand.StartsWith("free"))
                    {
                        parsed.Add(_bashCommand, ParseFree(bashResult));
                    }
                    else if (_bashCommand.StartsWith("netstat"))
                    {
                        parsed.Add(_bashCommand, ParseNetstat(bashResult));
                    }
                    else if (_bashCommand.StartsWith("dpkg -l"))
                    {
                        parsed.Add(_bashCommand, ParseDpkg(bashResult));
                    }
                    else if (_bashCommand.StartsWith("ifconfig -a"))
                    {
                        parsed.Add(_bashCommand, ParseIfConfig(bashResult));
                    }
                    else if (_bashCommand.StartsWith("lscpu") ||
                            _bashCommand.StartsWith("cat /proc/meminfo"))
                    {
                        parsed.Add(_bashCommand, ParseLscpu(bashResult));
                    }
                    else if (_bashCommand.StartsWith("ls") ||
                            _bashCommand.StartsWith("cat /proc/uptime") ||
                            _bashCommand.StartsWith("uname -r"))
                    {
                        parsed.Add(_bashCommand, bashResult.Result.Split(new char[] { ' ', '\t', '\n' }, StringSplitOptions.RemoveEmptyEntries));
                    }
                    else if (_bashCommand.StartsWith("iostat"))
                    {
                        parsed.Add(_bashCommand, ParseIostat(bashResult));
                    }
                    else if (_bashCommand.StartsWith("vmstat"))
                    {
                        parsed.Add(_bashCommand, ParseVmstat(bashResult));
                    }
                    else if (_bashCommand.StartsWith("uname -a"))
                    {
                        parsed.Add(_bashCommand, ParseUnameA(bashResult));
                    }
                    else if (_bashCommand.StartsWith("cat /proc/self/mountstats"))
                    {
                        parsed.Add(_bashCommand, ParseMountstats(bashResult));
                    }
                    else if (_bashCommand.StartsWith("cat /proc/diskstats") ||
                            _bashCommand.StartsWith("cat /proc/self/mounts"))
                    {
                        parsed.Add(_bashCommand, ParseListWithList(bashResult));
                    }
                    else if (_bashCommand.StartsWith("cat /proc/stat"))
                    {
                        parsed.Add(_bashCommand, ParseStat(bashResult));
                    }
                    else if (_bashCommand.StartsWith("cat /proc/version"))
                    {
                        parsed.Add(_bashCommand, ParseProcVersion(bashResult));
                    }


                    else //add unparsed result
                    {
                        errorsDictionary.Add("Exception", "Parsing not supported");
                        // var lines = bashResult.Result.Split('\n').ToList();
                        // lines.Remove("");
                        // parsed.Add(_bashCommand, lines);
                    }
                }

                catch (Exception e)
                {
                    errorsDictionary.Add("Exception", "Exception was thrown while parsing command: " + _bashCommand + "\nException: " + e.Message + "\nStackTrace: " + e.StackTrace);
                }
            }
            //add errors
            errorsDictionary.Add("Error", bashResult.Error);
            parsed.Add("Errors", errorsDictionary);

            return JsonConvert.SerializeObject(parsed);

        }

        public List<Dictionary<string, object>> ParseCsvLike(BashResult bashResult)
        {
            var csv = new List<string[]>();
            var lines = bashResult.Result.Split('\n');

            int lineCounter = 0;
            Regex regexDefault = new Regex(@"(\[.*\])|(\S+)");

            bool supportSpacesInRegex = false;
            if (Regex.IsMatch(lines[0], @".*?Name\s+Version\s+Architecture\s+Description.*"))
            {
                supportSpacesInRegex = true;
            }
            bool isNetstat = false;
            if (Regex.IsMatch(lines[0], @".*?Proto\s+Recv-Q\s+Send-Q\s+Local Address\s+Foreign Address\s+State.*"))
            {
                regexDefault = new Regex(@"(\[.*\])|(\S+)|\s{30,}");
                isNetstat = true;
            }
            Regex regex;
            foreach (string line in lines)
            {
                if (line != "")
                {
                    var lineTrimmed = line.TrimStart().TrimEnd();
                    
                    if (lineCounter == 0 && isNetstat)
                    {
                        //on the first line match string (symbol withespace symbol at least 2 whitspaces), hack for netstat
                        regex = new Regex(@"(\[.*\])|\S+\s\S+\s{2,}|(\S+)|\s{20,}");
                    }
                    else if (lineCounter != 0 && supportSpacesInRegex)
                    {
                        regex = new Regex(@"(\[.*\])|(\w+\s\S.*)|(\S+)");
                    }
                    else
                    {
                        regex = regexDefault;
                    }

                    var regexMatches = regex.Matches(lineTrimmed);
                    var matchedValues = new String[regexMatches.Count];
                    int i = 0;
                    foreach (Match match in regexMatches)
                    {
                        matchedValues[i] = match.Value.TrimEnd();
                        i++;
                    }
                    csv.Add(matchedValues);
                }
                lineCounter++;
            }

            var properties = csv[0];

            var listObjResult = new List<Dictionary<string, object>>();

            for (int i = 1; i < csv.Count; i++)
            {
                var objResult = new Dictionary<string, object>();
                for (int j = 0; j < properties.Length; j++)
                {
                    if (csv[i].Length > j)
                    {
                        if (objResult.ContainsKey(properties[j]))
                        {
                            var propsUnknownType = objResult[properties[j]];
                            List<string> propValues;
                            if (propsUnknownType.GetType() == typeof(List<string>))
                            {
                                propValues = propsUnknownType as List<string>;
                            }
                            else
                            {
                                propValues = new List<string>() { propsUnknownType as String };
                            }
                            propValues.Add(csv[i][j]);
                            objResult.Remove(properties[j]);
                            objResult.Add(properties[j], propValues);
                        }
                        else
                        {
                            objResult.Add(properties[j], csv[i][j]);
                        }
                    }

                }
                listObjResult.Add(objResult);
            }

            return listObjResult;
        }
        private Dictionary<string, object> ParseFree(BashResult bashResult)
        {
            var result = new Dictionary<string, object>();
            var csv = new List<string[]>();
            var lines = bashResult.Result.Split('\n');

            foreach (string line in lines)
            {
                if (line != "")
                {
                    csv.Add(line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries));
                }
            }
            var properties = csv[0];
            for (int i = 1; i < csv.Count; i++)
            {
                var line = csv[i];
                var propDictionary = new Dictionary<string, string>();
                for (int j = 0; j < properties.Length; j++)
                {
                    if (line.Length > j + 1)
                    {
                        propDictionary.Add(properties[j], line[j + 1]);
                    }
                }
                result.Add(line[0], (object)propDictionary);
            }
            return result;
        }
        private Dictionary<string, object> ParseTop(BashResult bashResult)
        {
            var result = new Dictionary<string, object>();
            var csv = new List<string[]>();
            var lines = bashResult.Result.Split('\n').ToList();
            if (String.IsNullOrWhiteSpace(lines[0]))
            {
                lines.RemoveAt(0); //remove first empty line
            }
            int emptyLinePosition = 0, i = 0;
            while (true)
            {
                if (String.IsNullOrWhiteSpace(lines[i]))
                {
                    emptyLinePosition = i;
                    break;
                }
                i++;
                if (i >= lines.Count)
                {
                    break;
                }
            }
            var preCsvLike = new List<string>();
            var csvLike = new List<string>();
            for (i = 0; i < emptyLinePosition; i++)
            {
                preCsvLike.Add(lines[i]);
            }
            for (i = emptyLinePosition + 1; i < lines.Count; i++)
            {
                csvLike.Add(lines[i]);
            }

            //add "header"
            //line1
            var line1 = preCsvLike[0].Split(",");
            var topSubstr = line1[0].Split('-');
            result.Add(topSubstr[0].Trim(), topSubstr[1].Trim());

            var users = line1[1].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            result.Add(users[1].Trim(), users[0].Trim());

            var loadAverage = line1[2].Split(": ");
            result.Add(loadAverage[0], new List<string>() { loadAverage[1].Trim(), line1[3].Trim(), line1[4].Trim() });

            //lines 2-5
            for (i = 1; i < preCsvLike.Count; i++)
            {
                var line = preCsvLike[i];
                var name = line.Split(':')[0];
                var propsLine = line.Split(':')[1];
                var props = Regex.Split(propsLine, @",|\.\s+");
                var propDictionary = new Dictionary<string, string>();
                foreach (var prop in props)
                {
                    var propTrimmed = prop.TrimStart().TrimEnd();
                    var propValue = propTrimmed.Split(' ')[0];
                    var propKey = Regex.Split(propTrimmed, @"\d+(\.?\d+)?").Last().TrimStart();
                    propDictionary.Add(propKey, propValue);
                }

                result.Add(name, propDictionary);
            }

            // add csv like processes output
            var br = new BashResult();

            foreach (var line in csvLike)
            {
                br.Result += line + "\n";
            }
            result.Add("Processes", ParseCsvLike(br));
            return result;
        }
        private Dictionary<string, object> ParseNetstat(BashResult bashResult)
        {
            var result = new Dictionary<string, object>();
            var activeSections = Regex.Split(bashResult.Result, @"Active.*?\n").ToList();
            activeSections.Remove("");
            var sectionNamesMatches = Regex.Matches(bashResult.Result, @"Active.*?\n");
            var sectionNames = new List<string>();
            foreach (Match match in sectionNamesMatches)
            {
                sectionNames.Add(match.Value);
            }
            if (sectionNames.Count != activeSections.Count)
            {
                throw new Exception("Cannot parse. There is probably a process with 'Active' name, which is not supported because of parsing method.");
            }
            int i = 0;
            foreach (var section in activeSections)
            {
                var fakeBashResult = new BashResult() { Result = section };
                result.Add(sectionNames[i], ParseCsvLike(fakeBashResult));
                i++;
            }

            return result;
        }
        private Dictionary<string, object> ParseIfConfig(BashResult bashResult)
        {
            var result = new Dictionary<string, object>();

            var nestedDicionary1 = new Dictionary<string, object>();
            var nestedDicionary2 = new Dictionary<string, object>();
            var nestedDicionaryKeys = new List<string>();
            var nestedRX1 = new Dictionary<string, string>();
            var nestedTX1 = new Dictionary<string, string>();
            var nestedRX2 = new Dictionary<string, string>();
            var nestedTX2 = new Dictionary<string, string>();
            int indexOfSecondDictionary = 0;

            var lines = bashResult.Result.Split('\n').ToList();


            nestedDicionaryKeys.Add(lines[0].Substring(0, lines[0].IndexOf(":")));
            lines[0] = lines[0].Replace(nestedDicionaryKeys[0] + ": ", "");

            var indexysToRemove = new List<int>();
            int index = 0;

            while (index < lines.Count)
            {
                if (String.IsNullOrWhiteSpace(lines[index]))
                    indexysToRemove.Add(index);

                index++;
            }

            for (int i = 0; i < indexysToRemove.Count - 1; i++)
            {
                if (indexysToRemove[i + 1] - indexysToRemove[i] > 1)
                {
                    nestedDicionaryKeys.Add(lines[indexysToRemove[i] + 1].Substring(0, lines[indexysToRemove[i] + 1].IndexOf(":")));
                    indexOfSecondDictionary = indexysToRemove[i];
                    lines[indexysToRemove[i] + 1] = lines[indexysToRemove[i] + 1].Replace(nestedDicionaryKeys[1 + i] + ": ", "");
                }
            }

            indexysToRemove.Reverse();

            foreach (int i in indexysToRemove)
            {
                lines.RemoveAt(i);
            }

            indexysToRemove = new List<int>();

            //remove the spaces at the begining of lines
            for (int i = 0; i < lines.Count; i++)
            {
                for (int j = 0; j < lines[i].Length; j++)
                {
                    if (lines[i][j] == Convert.ToChar(" "))
                    {
                        indexysToRemove.Add(j);
                    }
                    else
                    {
                        break;
                    }
                }
                indexysToRemove.Reverse();
                foreach (int k in indexysToRemove)
                {
                    lines[i] = lines[i].Remove(k, 1);
                }
                indexysToRemove = new List<int>();
            }

            string[] tempString;

            for (int i = 0; i < lines.Count; i++)
            {
                tempString = lines[i].Split(new char[] { ' ', '\t', '=' }, StringSplitOptions.RemoveEmptyEntries);
                SetKeysAndValues(tempString, i);
            }

            nestedDicionary1.Add("RX", nestedRX1);
            nestedDicionary2.Add("RX", nestedRX2);
            nestedDicionary1.Add("TX", nestedTX1);
            nestedDicionary2.Add("TX", nestedTX2);
            result.Add(nestedDicionaryKeys[0], nestedDicionary1);
            result.Add(nestedDicionaryKeys[1], nestedDicionary2);

            void SetKeysAndValues(string[] line, int indexOfLine)
            {
                string tempValue = "";

                for (int j = 0; j < line.Length; j++)
                {
                    if (line[0] == "RX" && indexOfLine < indexOfSecondDictionary)
                    {
                        tempValue = GetValueByKey(line[j], line);
                        if (tempValue != "")
                            nestedRX1.Add(line[j], tempValue);
                    }
                    else if (line[0] == "TX" && indexOfLine < indexOfSecondDictionary)
                    {
                        tempValue = GetValueByKey(line[j], line);
                        if (tempValue != "")
                            nestedTX1.Add(line[j], tempValue);
                    }
                    else if (line[0] != "TX" && line[0] != "RX" && indexOfLine < indexOfSecondDictionary)
                    {
                        tempValue = GetValueByKey(line[j], line);
                        if (tempValue != "")
                            nestedDicionary1.Add(line[j], tempValue);
                    }
                    else if (line[0] == "RX" && indexOfLine >= indexOfSecondDictionary)
                    {
                        tempValue = GetValueByKey(line[j], line);
                        if (tempValue != "")
                            nestedRX2.Add(line[j], tempValue);
                    }
                    else if (line[0] == "TX" && indexOfLine >= indexOfSecondDictionary)
                    {
                        tempValue = GetValueByKey(line[j], line);
                        if (tempValue != "")
                            nestedTX2.Add(line[j], tempValue);
                    }
                    else if (line[0] != "TX" && line[0] != "RX" && indexOfLine >= indexOfSecondDictionary)
                    {
                        tempValue = GetValueByKey(line[j], line);
                        if (tempValue != "")
                            nestedDicionary2.Add(line[j], tempValue);
                    }
                }

                string GetValueByKey(string key, string[] tempLine)
                {
                    string value = "";

                    switch (key)
                    {
                        case "flags":
                        case "mtu":
                        case "inet":
                        case "netmask":
                        case "broadcast":
                        case "inet6":
                        case "prefixlen":
                        case "scopeid":
                        case "ether":
                        case "packets":
                        case "errors":
                        case "dropped":
                        case "overruns":
                        case "frame":
                        case "carrier":
                        case "collisions":

                            for (int i = 0; i < tempLine.Length; i++)
                            {
                                if (tempLine[i] == key)
                                    value = tempLine[i + 1];
                            }
                            break;

                        case "txqueuelen":
                            for (int i = 0; i < tempLine.Length; i++)
                            {
                                if (tempLine[i] == key)
                                    value = tempLine[i + 1] + tempLine[i + 2];
                            }
                            break;

                        case "bytes":
                            for (int i = 0; i < tempLine.Length; i++)
                            {
                                if (tempLine[i] == key)
                                    value = tempLine[i + 1] + tempLine[i + 2] + tempLine[i + 3];
                            }
                            break;

                        case "loop":
                            for (int i = 1; i < tempLine.Length; i++)
                            {
                                value = value + tempLine[i];
                            }
                            break;
                    }

                    return value;
                }
            }

            return result;
        }

        private Dictionary<string, object> ParseLscpu(BashResult bashResult)
        {
            var result = new Dictionary<string, object>();

            var lines = bashResult.Result.Split('\n').ToList();
            lines.Remove("");
            string[] tempString;
            foreach (string s in lines)
            {
                tempString = Regex.Split(s, @":\s+");
                result.Add(tempString[0], tempString[1]);
            }

            return result;
        }


        private object ParseDpkg(BashResult bashResult)
        {
            var result = new Dictionary<string, object>();
            var regex = new Regex(@".*?Name\s+Version\s+Architecture\s+Description.*?\n");
            var sections = regex.Split(bashResult.Result).ToList();
            sections.Remove("");

            //header
            var headerRegex = new Regex(@"=.*?\n");
            var headerKeys = headerRegex.Split(sections[0]).ToList();
            headerKeys.Remove("");
            var headerValuesMatches = headerRegex.Matches(sections[0]);
            var headerValues = new List<string>();
            foreach (Match match in headerValuesMatches)
            {
                headerValues.Add(match.Value.Substring(1));
            }
            for (int i = 0; i < headerKeys.Count; i++)
            {
                result.Add(headerKeys[i], headerValues[i]);
            }
            //list
            //remove +++-==== line
            var listOfPackages = RemoveFirstLines(sections[1], 1);

            //add line with name, version architecture and description
            var csvStyleHeader = regex.Matches(bashResult.Result).First().Value;
            listOfPackages = csvStyleHeader + listOfPackages;

            var fakeBashResult = new BashResult() { Result = listOfPackages };
            result.Add(csvStyleHeader, ParseCsvLike(fakeBashResult));

            return result;
        }
        private static string RemoveFirstLines(string text, int linesCount)
        {
            var lines = Regex.Split(text, "\r\n|\r|\n").Skip(linesCount);
            return string.Join(Environment.NewLine, lines.ToArray());
        }

        private List<object> ParseIostat(BashResult bashResult)
        {
            var result = new List<object>();

            var bashResultLines = bashResult.Result.Split('\n').ToList();
            bashResultLines.Remove("");

            var regexFirstLine = new Regex(@"\s{2,}|\s?\t+\s?");
            var firstLineMaches = regexFirstLine.Split(bashResultLines[0]);
            foreach (var value in firstLineMaches)
            {
                result.Add(value);
            }

            //avgCpu
            var avgCpuName = bashResultLines[1].Split(':')[0];
            var avgCpuProps = bashResultLines[1].Substring(avgCpuName.Length + 1);
            avgCpuProps = avgCpuProps.TrimStart().TrimEnd();
            var fakeBashResult = new BashResult() { Result = avgCpuProps + "\n" + bashResultLines[2].TrimStart().TrimEnd() };
            var avgCpuDictionary = new Dictionary<string, object>();
            avgCpuDictionary.Add(avgCpuName, ParseCsvLike(fakeBashResult));
            result.Add(avgCpuDictionary);

            //list
            var bashResultWithoutEmptyLines = string.Join(Environment.NewLine, bashResultLines.ToArray());
            var listOfDevices = RemoveFirstLines(bashResultWithoutEmptyLines, 3);
            fakeBashResult = new BashResult() { Result = listOfDevices };
            result.Add(ParseCsvLike(fakeBashResult));

            return result;
        }
        private Dictionary<string, object> ParseVmstat(BashResult bashResult)
        {
            var result = new Dictionary<string, object>();
            var bashResultLines = bashResult.Result.Split('\n').ToList();
            bashResultLines.Remove("");

            var categories = bashResultLines[0].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            var props = bashResultLines[1].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            var values = bashResultLines[2].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

            int propCounter = 0;
            foreach (var category in categories)
            {
                var nameDictionary = new Dictionary<string, object>();
                int propsInCategory = 2;
                if (category.Contains("memory", StringComparison.InvariantCultureIgnoreCase))
                {
                    propsInCategory = 4;
                }
                else if (category.Contains("cpu", StringComparison.InvariantCultureIgnoreCase))
                {
                    propsInCategory = 5;
                }

                for (int i = propCounter; i < propCounter + propsInCategory; i++)
                {
                    nameDictionary.Add(props[i], values[i]);
                }
                propCounter += propsInCategory;

                result.Add(category, nameDictionary);
            }

            return result;
        }
        private List<object> ParseUnameA(BashResult bashResult)
        {
            var result = new List<object>();
            var values = bashResult.Result.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            result.Add(values[0]);
            result.Add(values[1]);
            result.Add(values[2]);
            result.Add(values[3] + " " +
                        values[4] + " " +
                        values[5] + " " +
                        values[6] + " " +
                        values[7] + " " +
                        values[8] + " " +
                        values[9] + " " +
                        values[10]);
            result.Add(values[11]);
            result.Add(values[12]);
            result.Add(values[13]);
            result.Add(values[14].TrimEnd());

            return result;
        }
        private List<object> ParseListWithList(BashResult bashResult)
        {
            var result = new List<object>();

            var lines = bashResult.Result.Split('\n').ToList();
            lines.Remove("");
            foreach (var line in lines)
            {
                result.Add(line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries).ToList());
            }

            return result;
        }
        private Dictionary<string, object> ParseStat(BashResult bashResult)
        {
            var result = new Dictionary<string, object>();

            var lines = bashResult.Result.Split('\n').ToList();
            lines.Remove("");
            foreach (var line in lines)
            {
                var values = line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                var name = values[0];
                values.Remove(name);
                result.Add(name, values);
            }

            return result;
        }

        private List<object> ParseProcVersion(BashResult bashResult)
        {
            var result = new List<object>();
            var regex = new Regex(@"(\(.*\))|(\S+)");
            var matches = regex.Matches(bashResult.Result);

            result.Add(matches[0].Value + " " +
                        matches[1].Value + " " +
                        matches[2].Value);
            result.Add(matches[3].Value);
            result.Add(matches[4].Value + " " +
                        matches[5].Value + " " +
                        matches[6].Value + " " +
                        matches[7].Value + " " +
                        matches[8].Value + " " +
                        matches[9].Value + " " +
                        matches[10].Value + " " +
                        matches[11].Value);

            return result;
        }

        private List<object> ParseMountstats(BashResult bashResult)
        {
            var result = new List<object>();

            var lines = bashResult.Result.Split('\n').ToList();
            lines.Remove("");
            var regex = new Regex(@"device |mounted on|with fstype");
            foreach (var line in lines)
            {
                var lineDictionary = new Dictionary<string, object>();
                var values = regex.Split(line).ToList();
                values.Remove("");
                var matches = regex.Matches(line);
                int i = 0;
                foreach (var value in values)
                {
                    lineDictionary.Add(matches[i].Value, value);
                    i++;
                }
                result.Add(lineDictionary);
            }

            return result;
        }
    }

}