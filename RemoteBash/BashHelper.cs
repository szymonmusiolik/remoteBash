/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <szymonmusiolik@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Szymon
 * ----------------------------------------------------------------------------
 */

using System;
using System.Diagnostics;

namespace RemoteBash
{
    public static class BashHelper
    {
        public static BashResult Bash(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();

            BashResult result;
            result.Result = process.StandardOutput.ReadToEnd();
            result.Error = process.StandardError.ReadToEnd();

            process.WaitForExit();

            return result;
        }
    }
    public struct BashResult
    {
        public string Result;
        public string Error;
    };
}