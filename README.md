This is a remote linux bash console with web browser access through websocket

How to run (linux machine only):
1. install dotnet core
2. cd to remoteBash
3. run command: dotnet restore 
4. run command: dotnet build
5. cd to remoteBash/RemoteBash
6. run command: dotnet run
7. open browser and go to <yourIp>:5000 (e.g. http://localhost:5000/)